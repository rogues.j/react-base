/* eslint-disable max-len */
import { useState } from 'react';

export default (init, delta) => {
  const [counter, setCounter] = useState(init);

  return [
    counter,
    () => setCounter(counter + delta),
    () => setCounter(counter - delta)
  ];
};
