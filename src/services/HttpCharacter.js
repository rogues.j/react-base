const getPage = (page) => fetch(`https://rickandmortyapi.com/api/character?page=${page}`)
  .then((response) => response.json())
  .then((body) => body.results);

export const all = async () => {
  const a = await Promise.all([...Array(2).keys()]
    .map((i) => i + 1)
    .map(getPage));
  return a.flat();
};

export default {
  all
};
