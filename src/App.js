import React, { useState } from 'react';
import {
  Button, Space, Divider, AutoComplete
} from 'antd';
import { FireTwoTone } from '@ant-design/icons';
import CharacterSmall from './components/CharacterSmall';
import CharacterSquare from './components/CharacterSquare';
import CharacterList from './components/CharacterList';
import './App.css';
import './Debug.css';

export default () => {
  const [search, setSearch] = useState('rick');

  return (
    <Space
      direction="vertical"
      className="pa2 tc w-100"
    >
      <Space className="pa2">
        <Button type={search === '' ? 'default' : 'primary'} onClick={() => setSearch('')}>Clear</Button>
        <Button type={search === 'rick' ? 'default' : 'primary'} onClick={() => setSearch('rick')}>Ricks</Button>
        <Button type={search === 'morty' ? 'default' : 'primary'} onClick={() => setSearch('morty')}>Morties</Button>
      </Space>
      <Space className="pa2">
        <AutoComplete
          options={[
            { value: 'rick' },
            { value: 'morty' },
            { value: 'beth' },
            { value: 'summer' },
            { value: 'jerry' }
          ].filter(({ value }) => value.toLowerCase().includes(search.toLowerCase()))}
          style={{ width: 200 }}
          onSelect={(data) => setSearch(data)}
          onSearch={(data) => setSearch(data)}
          prefix={<FireTwoTone twoToneColor="#eb2f96" />}
          placeholder="search..."
          value={search}
        />
      </Space>
      <Space direction="vertical" className="pa2 w-100">
        <Divider>Preview</Divider>
        <CharacterList Display={CharacterSmall} filter={search} />
        <Divider>List</Divider>
        <CharacterList Display={CharacterSquare} filter={search} />
      </Space>
    </Space>
  );
};
