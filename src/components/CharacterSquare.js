/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import {
  atom,
  useRecoilState
} from 'recoil';
import useCounter from '../hooks/useCounter';

export default ({ id, name, image }) => {
  const [counter, incr] = useCounter(0, 1);

  const charCounter = atom({
    key: `charCounter.${id}`,
    default: 0
  });

  const [globalCounter, setGlobalCounter] = useRecoilState(charCounter);

  return (
    <div
      style={{
        width: '100px'
      }}
      className="ma1"
    >
      <img
        alt="img"
        src={image}
        onClick={() => {
          incr();
          setGlobalCounter(globalCounter + 1);
        }}
      />
      <div className="truncate">{name}</div>
      <div>{`${counter}/${globalCounter}`}</div>
    </div>
  );
};
