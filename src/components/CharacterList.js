/* eslint-disable max-len */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, { useState, useMemo, useEffect } from 'react';
import { Space } from 'antd';
import HttpCharacter from '../services/HttpCharacter';

const filterBy = (arr, attr, filter) => arr.filter((obj) => obj[attr].toLowerCase().includes(filter.toLowerCase()));

export default ({ filter, Display }) => {
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    HttpCharacter.all().then((arr) => setCharacters(arr));
  });

  const filteredList = useMemo(() => filterBy(characters, 'name', filter), [characters, filter]);

  return (
    <Space
      className="pa2"
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
      }}
    >
      {filteredList.map(({ id, name, image }) => (
        <Display key={id} id={id} name={name} image={image} />
      ))}
    </Space>
  );
};
