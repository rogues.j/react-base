import React from 'react';
import {
  atom,
  useRecoilState
} from 'recoil';

export default ({ id, image }) => {
  const charCounter = atom({
    key: `charCounter.${id}`,
    default: 0
  });

  const [globalCounter, setGlobalCounter] = useRecoilState(charCounter);

  return (
    <div>
      <img onClick={() => setGlobalCounter(globalCounter + 1)} style={{ width: '30px' }} src={image} alt="img" />
    </div>
  );
};
